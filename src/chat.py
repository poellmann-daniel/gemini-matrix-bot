import google.generativeai as genai
import uuid


class Chat:

    def __init__(self, api_key, event_id=None):
        if event_id is None:
            self.event_id = str(uuid.uuid4())
        else:
            self.event_id = event_id
        genai.configure(api_key=api_key)
        model = genai.GenerativeModel('gemini-pro')

        self.chat = model.start_chat()

    def say(self, message):
        try:
            response = self.chat.send_message(message).text
        except Exception as e:
            response = f"There was an issue with calling the Gemini API.\n(Error: {e})"
        return response
